<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $clients = \App\Client::select('*');
            return DataTables::of($clients)
                ->editColumn('avatar', function ($query) {
                    return asset('storage/images') . '/' . $query->avatar;
                })
                ->addColumn('action', function ($query) {
                    $btn = '<a href="' . route('client.edit', $query->id) . '" class="btn dt-custom-button"><i class="fa fa-pencil fa-lg"></i> </a>
                            <a href="javascript:void(0)" onclick="deleteClient(' . $query->id . ')" class="btn dt-custom-button"><i class="fa fa-trash fa-lg"></i> </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('client.index');


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=100,min_height=100',
        ]);


        $client = new Client();
        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $fileName = time() . '.' . $avatar->getClientOriginalExtension();
            Storage::disk('public')->put('images' . '/' . $fileName, File::get($avatar));
            $client->avatar = $fileName;
        }
        $client->email = $request->email;
        $client->save();


        return redirect(route('client.index'))->with('success', 'Client Added Successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Client $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        $client->avatar = asset('storage/images') . '/' . $client->avatar;
        return view('client.edit', ['client' => $client]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Client $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {

        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
//            'avatar'=>'required|image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=100,min_height=100',
        ]);

        //update record in database

        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $fileName = time() . '.' . $avatar->getClientOriginalExtension();
            Storage::disk('public')->put('images' . '/' . $fileName, File::get($avatar));
            $client->avatar = $fileName;
        }
        $client->email = $request->email;
        $client->save();


        return redirect(route('client.index'))->with('success', 'Client Updated Successfully.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Client $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {


        if ($client->avatar != 'default.png') {
            //Remove file from storage when client delete
            unlink(storage_path('app/public/images') . '/' . $client->avatar);
        }

        //delete record from database
        $client->delete();

        return response()->json([
            'success' => true,
            'message' => 'Client deleted successfully!'
        ]);
    }
}
