<?php

namespace App\Http\Controllers;

use App\Client;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $transactions = \App\Transaction::leftJoin('clients', 'transactions.client_id', '=', 'clients.id')
                ->select(
                    'transactions.*',
                    \Illuminate\Support\Facades\DB::raw("CONCAT(clients.first_name,' ',clients.last_name) as client_name")
                );
            return DataTables::of($transactions)
                ->editColumn('client_name', function ($query) {
                    return $query->client_name;
                })
                ->editColumn('transaction_date', function ($query) {
                    return Carbon::parse($query->transaction_date)->format('Y-m-d');
                })
                ->addColumn('action', function ($query) {
                    $btn = '<a href="' . route('transaction.edit', $query->id) . '" class="btn dt-custom-button"><i class="fa fa-pencil fa-lg"></i> </a>
                            <a href="javascript:void(0)" onclick="deleteTransaction(' . $query->id . ')" class="btn dt-custom-button"><i class="fa fa-trash fa-lg"></i> </a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('transaction.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = Client::get();
        return view('transaction.create', ['clients' => $clients]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'client_id' => 'required|not_in:0',
            'amount' => 'required',
            'transaction_date' => 'required'
        ]);

        Transaction::create([
            'client_id' => $request->client_id,
            'amount' => $request->amount,
            'transaction_date' => $request->transaction_date,
        ]);

        return redirect(route('transaction.index'))->with('success', 'Transaction Added Successfully.');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Transaction $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        $clients = Client::get();
        return view('transaction.edit', ['transaction' => $transaction, 'clients' => $clients]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Transaction $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {

        $this->validate($request, [
            'client_id' => 'required|not_in:0',
            'amount' => 'required',
            'transaction_date' => 'required'
        ]);

        $transaction->client_id = $request->client_id;
        $transaction->amount = $request->amount;
        $transaction->transaction_date = $request->transaction_date;
        $transaction->save();

        return redirect(route('transaction.index'))->with('success', 'Transaction Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Transaction $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        // delete transaction from database
        $transaction->delete();

        return response()->json([
            'success' => true,
            'message' => 'Transaction deleted successfully!'
        ]);
    }
}
