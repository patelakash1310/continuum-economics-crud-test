## Continuum Economics - CRUD application With TDD

## Task List
1. List all Clients.
2. Create/Update/Delete Clients
3. List all Transaction
4. Create/Update/Delete Transaction
5. Test case for Client Functionality
6. Test case for Transaction Functionality
 
 
 
## Steps for Installation

- Clone repository using git clone https://gitlab.com/patelakash1310/continuum-economics-crud-test.git
- Rename .env.example file to .env your project root and fill the database information. (windows wont let you do it, so you have to open your console cd your project root directory and run mv .env.example .env )
- Open the console and cd your project root directory
- Run composer install or php composer.phar install
- Run php artisan key:generate
- Run php artisan migrate
- Run php artisan db:seed to run seeders
- Run php artisan serve


## Default Credentials

Email : admin@admin.com
Password : password


## Below is Test case list
- Test Create Client run this command .vendor/bin/phpunit --filter testCreateClient
- Test Create Client run this command .vendor/bin/phpunit --filter testUpdateClient
- Test Create Client run this command .vendor/bin/phpunit --filter testDeleteClient

- Test Create Client run this command .vendor/bin/phpunit --filter testCreateTransaction
- Test Create Client run this command .vendor/bin/phpunit --filter testUpdateTransaction
- Test Create Client run this command .vendor/bin/phpunit --filter testDeleteTransaction
