<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(\App\User::count() == 0)
        {
            \App\User::create([
                'name'=>'Admin',
                'email'=>'admin@admin.com',
                'password'=>bcrypt('password'),
            ]);
        }
    }
}
