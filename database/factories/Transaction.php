<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Transaction::class, function (Faker $faker) {
    return [
        'client_id'=> function () {
            return factory(App\Client::class)->create()->id;
        },
        'amount'=>$faker->randomNumber(3),
        'transaction_date'=>$faker->dateTime($max = 'now', $timezone = null),
    ];
});
