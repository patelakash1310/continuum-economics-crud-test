<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $article = factory('App\Client')->create();

        //When user visit the articles page
        $response = $this->get('/Client'); // your route to get article

        //He should be able to read the articles
        $response->assertSee($article->first_name);

    }
}
