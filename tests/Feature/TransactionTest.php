<?php

namespace Tests\Feature;

use App\Client;
use App\Transaction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TransactionTest extends TestCase
{

    use WithFaker;
    use RefreshDatabase;

    /**
     * A feature test create client functionality.
     *
     * @return void
     */
    public function testCreateTransaction()
    {
        //create transaction using faker
        $transaction = factory(\App\Transaction::class,2)->create();

        //When user submits transaction request to create endpoint
        $this->post('transaction',$transaction->toArray()); // your route to create client

        //It gets stored in the database
        $this->assertEquals(2,Transaction::all()->count());

    }

    /**
     * A feature test update transaction functionality.
     *
     * @return void
     */
    public function testUpdateTransaction()
    {
        //create client using faker
        $createTransactions = factory(\App\Transaction::class, 2)->create();

        // get Transaction for update record
        $transaction = Transaction::first();
        $transaction->amount = 120;

        //The transaction should be updated in the database.
        $this->put(route('transaction.update', $transaction->id), $transaction->toArray());

        //Assert that a transactions table in the database contains the updated data.
        $this->assertDatabaseHas('transactions', ['id' => $transaction->id, 'amount' => 120]);
    }

    /**
     * A feature test delete client functionality.
     *
     * @return void
     */
    public function testDeleteTransaction()
    {

        //a transaction which is created by the user
        $createTransaction = factory(\App\Transaction::class, 1)->create();

        //get transaction
        $transaction = Transaction::first();

        //When the user hit's the endpoint to delete the transaction
        $this->delete(route('transaction.destroy', $transaction->id));

        //Assert that a table transaction in the database does not contain the transaction.
        $this->assertDatabaseMissing('transactions', ['id' => $transaction->id]);
    }
}
