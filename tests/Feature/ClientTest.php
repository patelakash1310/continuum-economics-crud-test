<?php

namespace Tests\Feature;

use App\CategoryModel;
use App\Client;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Faker\Generator as Faker;

class ClientTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    /**
     * A feature test create client functionality.
     *
     * @return void
     */
    public function testCreateClient()
    {
        //create client using faker
        $client = factory(\App\Client::class, 2)->create();

        //When user submits client request to create endpoint
        $this->post('client', $client->toArray());

        //It gets stored in the database
        $this->assertEquals(2, Client::all()->count());

    }

    /**
     * A feature test update client functionality.
     *
     * @return void
     */
    public function testUpdateClient()
    {
        //create client using faker
        $client = factory(\App\Client::class, 2)->create();

        // get client for update record
        $client = Client::first();
        $client->first_name = 'Updated Name';

//        The client should be updated in the database.
        $this->put(route('client.update', $client->id), $client->toArray());// your route to update article

        //Assert that a Clients table in the database contains the updated data.
        $this->assertDatabaseHas('clients', ['id' => $client->id, 'first_name' => 'Updated Name']);
    }


    /**
     * A feature test delete client functionality.
     *
     * @return void
     */
    public function testDeleteClient()
    {

        //And a task which is created by the user
        $client = factory(\App\Client::class, 1)->create();

        //get Client
        $client = Client::first();

        //When the user hit's the endpoint to delete the client
        $this->delete(route('client.destroy', $client->id));

        //Assert that a table clients in the database does not contain the client.
        $this->assertDatabaseMissing('clients', ['id' => $client->id]);
    }

}
