@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">


        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="w-75">Clients</h4>
                        </div>
                        <div class="col-md-2 float-right">
                            <a href="{{route('client.create')}}" class="btn btn-primary">Add New Client</a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-bordered" id="client-table">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Avtar</th>
                            <th>Email</th>
                            <th width="12%">Action</th>

                        </tr>
                        </thead>
                    </table>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

    <script>
        $(function() {
            var table = $('#client-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('client.index') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'first_name', name: 'first_name' },
                    { data: 'last_name', name: 'last_name' },
                    { data: 'avatar', name: 'avatar',orderable: false,
                        "render": function (data, type, full, meta) {
                            return '<img src="'+ data +'" height="25"/>';
                        },},
                    { data: 'email', name: 'email' },
                    { data: 'action', name: 'action',orderable: false }
                ]
            });


        });

        function deleteClient(id) {

            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url : "{{ url('client')}}" + '/' + id,
                            type : "POST",
                            data : {'_method' : 'DELETE',_token : '{!! csrf_token() !!}'},
                            success: function(response){
                                swal(response.message, {
                                    icon: "success",
                                });
                                var table = $('#client-table').DataTable();
                                table.ajax.reload();

                            },
                            error : function(){
                                swal({
                                    title: 'Opps...',
                                    text : response.message,
                                    type : 'error',
                                    timer : '1500'
                                })
                            }
                        })
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
        }


    </script>
    @if(\Illuminate\Support\Facades\Session::has('success'))
        <script>
            swal("Good job!", "{{Session::get('success')}}", "success")
        </script>

    @endif
@endsection