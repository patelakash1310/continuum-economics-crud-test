@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="w-75">Edit Clients</h4>
                        </div>
                    </div>
                </div>
                <form method="post" action="{{route('client.update',$client)}}" enctype="multipart/form-data">
                <div class="card-body">
                    <div class="row">
                            {{csrf_field()}}
                        {{method_field('PATCH')}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName">First Name</label>
                                    <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Enter your First Name" value="{{$client->first_name}}">
                                    @if($errors->has('first_name'))
                                        <small class="text-danger">
                                            {{$errors->first('first_name')}}
                                        </small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="lastName">Last Name</label>
                                    <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter your Last Name" value="{{$client->last_name}}">
                                    @if($errors->has('last_name'))
                                        <small class="text-danger">
                                            {{$errors->first('last_name')}}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email">Email address</label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter your Email" value="{{$client->email}}">
                                    @if($errors->has('email'))
                                        <small class="text-danger">
                                            {{$errors->first('email')}}
                                        </small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">

                                <div class="col-md-2">
                                    <img src="{{$client->avatar}}" width="100" height="100">
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="avatar">Select Avatar</label>
                                        <input type="file" class="form-control-file" id="avatar" name="avatar">
                                        @if($errors->has('avatar'))
                                            <small class="text-danger">
                                                {{$errors->first('avatar')}}
                                            </small>
                                        @endif
                                    </div>
                                </div>
                            </div>


                    </div>
                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Save</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light m-r-10">Back</button>

                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection