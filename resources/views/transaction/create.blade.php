@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="w-75">Create Clients</h4>
                        </div>
                    </div>
                </div>
                <form method="post" action="{{route('transaction.store')}}">
                    {{csrf_field()}}
                <div class="card-body">
                    <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="firstName">Select Client</label>
                                    <select class="form-control" id="client_id" name="client_id">
                                        <option value="">-- Select Client --</option>
                                       @foreach($clients as $client)
                                           <option value="{{$client->id}}">{{$client->first_name}} {{$client->last_name}}</option>
                                       @endforeach
                                    </select>
                                    @if($errors->has('client_id'))
                                        <small class="text-danger">
                                            {{$errors->first('client_id')}}
                                        </small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="lastName">Amount</label>
                                    <input type="number" name="amount" class="form-control" id="amount" placeholder="Enter Amount" value="{{old('last_name')}}">
                                    @if($errors->has('amount'))
                                        <small class="text-danger">
                                            {{$errors->first('amount')}}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email">Transaction Date</label>
                                    <input type="date" class="form-control" name="transaction_date" id="transaction_date" placeholder="Enter Transaction Date" value="{{old('email')}}">
                                    @if($errors->has('transaction_date'))
                                        <small class="text-danger">
                                            {{$errors->first('transaction_date')}}
                                        </small>
                                    @endif
                                </div>
                            </div>

                    </div>
                </div>
                <div class="card-footer text-right">
                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Save</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light m-r-10">Back</button>

                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection