@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">


        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="w-75">Transactions</h4>
                        </div>
                        <div class="col-md-3 float-right">
                            <a href="{{route('transaction.create')}}" class="btn btn-primary">Add New Transaction</a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-bordered" id="transaction-table">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Client Name</th>
                            <th>Amount</th>
                            <th>Transaction Date</th>
                            <th width="12%">Action</th>

                        </tr>
                        </thead>
                    </table>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

    <script>
        $(function() {
            var table = $('#transaction-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('transaction.index') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'client_name', name: 'client_name' },
                    { data: 'amount', name: 'amount',
                        "render": function (data, type, full, meta) {
                            return '£'+data;
                        },
                    },
                    { data: 'transaction_date', name: 'transaction_date'},
                    { data: 'action', name: 'action',orderable: false }
                ]
            });


        });

        function deleteTransaction(id) {

            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url : "{{ url('transaction')}}" + '/' + id,
                            type : "POST",
                            data : {'_method' : 'DELETE',_token : '{!! csrf_token() !!}'},
                            success: function(response){
                                swal(response.message, {
                                    icon: "success",
                                });
                                var table = $('#transaction-table').DataTable();
                                table.ajax.reload();

                            },
                            error : function(){
                                swal({
                                    title: 'Opps...',
                                    text : response.message,
                                    type : 'error',
                                    timer : '1500'
                                })
                            }
                        })
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                });
        }


    </script>
    @if(\Illuminate\Support\Facades\Session::has('success'))
        <script>
            swal("Good job!", "{{Session::get('success')}}", "success")
        </script>

    @endif
@endsection